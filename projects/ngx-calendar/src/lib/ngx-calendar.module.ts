import { NgModule } from '@angular/core';
import { NgxCalendarComponent } from './ngx-calendar.component';



@NgModule({
  declarations: [
    NgxCalendarComponent
  ],
  imports: [
  ],
  exports: [
    NgxCalendarComponent
  ]
})
export class NgxCalendarModule { }
