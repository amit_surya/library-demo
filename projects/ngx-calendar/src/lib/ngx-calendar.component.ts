import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'lib-ngx-calendar',
  template: `
  <div class="calendar">
  <div class="calendar_picture">
    <h2>18, Sunday</h2>
    <h3>November</h3>
  </div>
  <div class="calendar_date">
    <div class="calendar_day">M</div>
    <div class="calendar_day">T</div>
    <div class="calendar_day">W</div>
    <div class="calendar_day">T</div>
    <div class="calendar_day">F</div>
    <div class="calendar_day">S</div>
    <div class="calendar_day">S</div>
    <div class="calendar_number"></div>
    <div class="calendar_number"></div>
    <div class="calendar_number"></div>
    <div class="calendar_number">1</div>
    <div class="calendar_number">2</div>
    <div class="calendar_number">3</div>
    <div class="calendar_number">4</div>
    <div class="calendar_number">5</div>
    <div class="calendar_number">6</div>
    <div class="calendar_number">7</div>
    <div class="calendar_number">8</div>
    <div class="calendar_number">9</div>
    <div class="calendar_number">10</div>
    <div class="calendar_number">11</div>
    <div class="calendar_number">12</div>
    <div class="calendar_number">13</div>
    <div class="calendar_number">14</div>
    <div class="calendar_number">15</div>
    <div class="calendar_number">16</div>
    <div class="calendar_number">17</div>
    <div class="calendar_number calendar_number --current">18</div>
    <div class="calendar_number">19</div>
    <div class="calendar_number">20</div>
    <div class="calendar_number">21</div>
    <div class="calendar_number">22</div>
    <div class="calendar_number">23</div>
    <div class="calendar_number">24</div>
    <div class="calendar_number">25</div>
    <div class="calendar_number">26</div>
    <div class="calendar_number">27</div>
    <div class="calendar_number">28</div>
    <div class="calendar_number">29</div>
    <div class="calendar_number">30</div>
  </div>
</div>
  `,
  styles: [
    `.calendar {
      position: relative;
      width: 300px;
      box-sizing: border-box;
      box-shadow: 0 5px 50px rgba(#000, 0.5);
      overflow: hidden;
    }
    .calendar_picture {
      position: relative;
      height: 200px;
      padding: 20px;
      color: #fff;
      background: #32ede2;
      text-shadow: 0 2px 2px rgba(#000, 0.2);
      box-sizing: border-box;
      
      &::before {
        content: "";
        left: 0;
        right: 0;
        top: 0;
        bottom: 0;
        position: absolute;
        background: linear-gradient(to top, rgba(#000, 0.25), rgba(#000, 0.1));
      }
      
      h2 {
        margin: 0;
      }
      
      h3 {
        margin: 0;
        font-weight: 500;
      }
    }
    
    .calendar_date {
      padding: 20px;
      display: grid;
      grid-template-columns: repeat(auto-fit, minmax(25px, 1fr));
      grid-gap: 10px;
      box-sizing: border-box;
    }
    
    .calendar_day {
      display: flex;
      align-items: center;
      justify-content: center;
      height: 25px;
      font-weight: 600;
      color: #262626;
      
      &:nth-child(7) {
        color: #ff685d;
      }
    }
    
    .calendar_number {
      display: flex;
      align-items: center;
      justify-content: center;
      height: 25px;
      color: #262626;
    
      &:nth-child(7n) {
        color: #ff685d;
        font-weight: 700;
      }
      
      &--current,
      &:hover {
        background-color: #009688;
        color: #fff !important;
        font-weight: 700;
        cursor: pointer;
        border-radius: 50px;
      }
    }
    `
  ]
})
export class NgxCalendarComponent implements OnInit {
  counter:number = 0;
  constructor() { }

  ngOnInit(): void {
  }
  onPlusBtnClick() {
    this.counter++;
    }
    onMinusBtnClick() {
    this.counter -- ;
    }
}
